﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void PlayerControllerScript::Start()
extern void PlayerControllerScript_Start_m9B0715BA4FD6DEC8F1B9AB08469C6C852865AF46 (void);
// 0x00000002 System.Void PlayerControllerScript::InitUDP()
extern void PlayerControllerScript_InitUDP_m773C36B796FAD55309D782B6610B84F72F8CC731 (void);
// 0x00000003 System.Void PlayerControllerScript::ReceiveData()
extern void PlayerControllerScript_ReceiveData_m0A924F2CE92FE0E578E0B5C58DD8CC8826F70E7D (void);
// 0x00000004 System.Void PlayerControllerScript::Jump()
extern void PlayerControllerScript_Jump_mEAD635E57AE39E880DA4E46CF5E52411A3129256 (void);
// 0x00000005 System.Void PlayerControllerScript::Run()
extern void PlayerControllerScript_Run_m461E2B18086EE2FB215068DE0B2CFE5730B08025 (void);
// 0x00000006 System.Void PlayerControllerScript::Update()
extern void PlayerControllerScript_Update_mE41BD5B970EAD886E99583B0D4C6BB85A5C8DD20 (void);
// 0x00000007 System.Void PlayerControllerScript::.ctor()
extern void PlayerControllerScript__ctor_m4DF29950ECEA42CF25A6D259F4C5CE3E02285595 (void);
static Il2CppMethodPointer s_methodPointers[7] = 
{
	PlayerControllerScript_Start_m9B0715BA4FD6DEC8F1B9AB08469C6C852865AF46,
	PlayerControllerScript_InitUDP_m773C36B796FAD55309D782B6610B84F72F8CC731,
	PlayerControllerScript_ReceiveData_m0A924F2CE92FE0E578E0B5C58DD8CC8826F70E7D,
	PlayerControllerScript_Jump_mEAD635E57AE39E880DA4E46CF5E52411A3129256,
	PlayerControllerScript_Run_m461E2B18086EE2FB215068DE0B2CFE5730B08025,
	PlayerControllerScript_Update_mE41BD5B970EAD886E99583B0D4C6BB85A5C8DD20,
	PlayerControllerScript__ctor_m4DF29950ECEA42CF25A6D259F4C5CE3E02285595,
};
static const int32_t s_InvokerIndices[7] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	7,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
